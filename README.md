## Rodar o comando

`$ npm install` (vai instalar as bibliotecas necessárias para rodar o código);

## Certificado

Incluir o certificado com o nome "MDICGOVBR.p12" na pasta "arquivos/certificados" (o nome pode ser alterado nas configurações)

## Incluir as váriveis de ambiente:

- senha_cert_prod (senha do certificado de produção)
- system_code_prod (system code da produção)

## Rodar os scripts

Extração completa:

`offset={offset} node extracaoCompleta.js` vai extrair os LPCOs do offset (eles são salvos no arquivo "{numeroDoOffset}.json"), offset é opcional, se rodar o comando sem ele, o padrão é 0

Se o script falhar em algum momento, basta rodar do offset que falhou para frente.


Obs.: Os arquivos gerados são salvos salvos na pasta "arquivos"


Extração diária:
`node extracaoDiaria.js` vai estrair os LPCOs da data anterior à que o script estiver rodando.

Observar que a extração completa salva os arquivos com o nome {offset}.json e a extração diária salva com o nome {data-da-extracao}.js

## Opcional:

Editando o arquivo utils/config.js é possível alterar alguns parâmetros (os nomes dos certificados)