'use strict'

const request = require('request');
const fs = require('fs');
const Q = require('q');
const configs = require('./config.js').parametros;

const abrirArquivo = fs.readFileSync;
const pastaCerts = configs["pastaCertificados"];
const ambiente = configs.ambiente;

let options;
if (ambiente === "hom") {

	options = {
		agentOptions: {
			ca: [abrirArquivo(`${pastaCerts}/icp.cer`),abrirArquivo(`${pastaCerts}/serprov3.cer`),abrirArquivo(`${pastaCerts}/serproacfv3.cer`)],
			pfx: abrirArquivo(`${pastaCerts}/${configs["nome-certificado"].hom}`),
			passphrase: configs["senha-certificado"].hom
		},
		headers: {
			'System-Code': configs['system-code'].hom
		},
		timeout: 120000
	}
}
if (ambiente === "prod") {
	options = {
		agentOptions: {
			pfx: abrirArquivo(`${pastaCerts}/${configs["nome-certificado"].prod}`),
			passphrase: configs["senha-certificado"].prod
		},
		headers: {
			'System-Code': configs['system-code'].prod
		},
		timeout: 120000
	}
}

exports.get = async function get (tokens, url) {
	let q = Q.defer();
	
	options.headers['Authorization'] = tokens.setToken;
	options.headers['X-CSRF-Token'] = tokens.csrfToken;
	options.url = url;

	request.get(options, function (err, resp, body) {
		if (err) {
			return q.reject(err.code);
		}

		const headers = resp.headers;
		const statusCode = resp.statusCode;
		if (statusCode >= 400) return q.reject(statusCode);

		q.resolve({headers, body, statusCode});
	});

	return q.promise;
}