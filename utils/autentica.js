'use strict'

const request = require('request');
const fs = require('fs');
const Q = require('q');
const configs = require('./config.js').parametros;

const abrirArquivo = fs.readFileSync;
const pastaCerts = configs["pastaCertificados"];
const ambiente = configs.ambiente;

let options;
if (ambiente === "hom") {

	options = {
		url: `${configs["url-base"].hom}/portal/api/autenticar/sistema`,
		agentOptions: {
			ca: [abrirArquivo(`${pastaCerts}/icp.cer`),abrirArquivo(`${pastaCerts}/serprov3.cer`),abrirArquivo(`${pastaCerts}/serproacfv3.cer`)],
			pfx: abrirArquivo(`${pastaCerts}/${configs["nome-certificado"].hom}`),
			passphrase: configs["senha-certificado"].hom
		},
		headers: {
			'System-Code': configs['system-code'].hom
		}
	}
}
if (ambiente === "prod") {
	options = {
		url: `${configs["url-base"].prod}/portal/api/autenticar/sistema`,
		agentOptions: {
			pfx: abrirArquivo(`${pastaCerts}/${configs["nome-certificado"].prod}`),
			passphrase: configs["senha-certificado"].prod
		},
		headers: {
			'System-Code': configs['system-code'].prod
		}
	}
}

function autenticar (dados) {

	let q = Q.defer();

	request.post(options, function (err, resp, body) {

		if (err) return q.reject(err);

		const statusCode = resp.statusCode;
		if (statusCode !== 200) {
			let mensagem = JSON.parse(body).message;
			q.reject(mensagem);
		}

		const headers = resp.headers;

		q.resolve({set: headers['set-token'], csrf: headers['x-csrf-token']});
	});

	return q.promise;
};

exports.pegaTokens = async function pegaTokens (dados) {
	try {
		const tokens = await autenticar(dados || {});

		const set = tokens.set;
		const csrf = tokens.csrf;

		return {setToken: set, csrfToken: csrf};
	} catch (e) {
		throw(e);
	}
}