'use strict'

const fs = require('fs');
const pegaTokens = require('./utils/autentica.js').pegaTokens;
const get = require('./utils/get.js').get;
const configs = require('./utils/config.js').parametros;

const ambiente = configs.ambiente;
let offset = Number(process.env.offset || 0);
const diretorioArquivo = configs.pastaArquivos;

let	urlBase;
let	systemCode;

let dataObj = new Date();
dataObj.setDate(dataObj.getDate() - 1);
let ontem = dataObj.toISOString().substring(0, 10);

let data = process.env.data || ontem;

if (ambiente === "hom") {
	urlBase = configs["url-base"].hom;
	systemCode = configs['system-code'].hom;
}
if (ambiente === "prod") {
	urlBase = configs["url-base"].prod;	
	systemCode = configs['system-code'].prod;
}

const urlConsulta = `${urlBase}/talpco/api/ext/lpco/consulta-completa?data-inicial=${data}&data-final=${data}&offset=`;

async function main () {
	console.log(`Autenticando em ${ambiente}`);
	let tokens;

	try {
		tokens = await pegaTokens({systemCode, urlBase});
		tokens.systemCode = systemCode;
	} catch (e) {
		return console.error(`Erro na autenticação: ${e}`);
	}

	console.log(`Extraindo todos os LPCOs de ${data}`);
	console.log(`Consultando o offset ${offset}...`);
	let chamada;
	let lpcos = [];
	try {
		chamada = await get(tokens, `${urlConsulta}${offset}`);
		lpcos = chamada.body;
		if (!lpcos) return console.log(`Não há LPCOs registrados em ${data}`);

		// fs.writeFileSync(`${diretorioArquivo}/${data}.json`, lpcos);
		fs.writeFileSync(`${diretorioArquivo}/extracaoDiaria.json`, lpcos);
	} catch (e) {
		return console.log(`Erro ${e}, repita o offset ${offset}`);
	}

	const numeroTotalLpcos = chamada.headers['content-range'].split('/')[1];
	const paginasRestantes = Math.ceil((numeroTotalLpcos - (Number(chamada.headers['content-range'].split('-')[1].split('/')[0].trim()) - 1))/500);

	if (paginasRestantes > 0) {
		console.log(`${paginasRestantes} página(s) restante(s).`);
		for (let i = 1; i <= paginasRestantes; i++) {
			offset += 500;
			console.log(`Consultando o offset ${offset} página ${i} de ${paginasRestantes}...`);

			try {
				chamada = await get(tokens, `${urlConsulta}${offset}`);
				lpcos = chamada.body;
				fs.appendFileSync(`${diretorioArquivo}/extracaoDiaria.json`, lpcos);
				// fs.appendFileSync(`${diretorioArquivo}/${data}.json`, lpcos);
			} catch (e) {
				console.log(`Erro ${e}, repita o offset ${offset}`)
			}
		}

	} else {
		console.log(`Foi salvo o arquivo extracaoDiaria.json`);
		return console.log('Extração completa');
	}
}

main();