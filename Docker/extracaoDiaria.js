'use strict'

const fs = require('fs');
const pegaTokens = require('./utils/autentica.js').pegaTokens;
const get = require('./utils/get.js').get;
const configs = require('./utils/config.js').parametros;

const ambiente = process.env.ENV || 'prod';
let offset = Number(process.env.offset || 0);

let	urlBase;
let	systemCode;

let data = new Date();
data.setDate(data.getDate() - 1);
let ontem = data.toISOString().substring(0, 10);

if (ambiente === "hom") {
	urlBase = configs["url-base"].hom;
	systemCode = configs['system-code'].hom;
}
if (ambiente === "prod") {
	urlBase = configs["url-base"].prod;	
	systemCode = configs['system-code'].prod;
}

const urlConsulta = `${urlBase}/talpco/api/ext/lpco/consulta-completa?data-inicial=${ontem}&data-final=${ontem}&offset=`;

async function main () {
	console.log(`Autenticando em ${ambiente}`);
	let tokens;

	try {
		tokens = await pegaTokens({systemCode, urlBase});
		tokens.systemCode = systemCode;
	} catch (e) {
		return console.error(`Erro na autenticação: ${e}`);
	}

	console.log(`Extraindo todos os LPCOs de ${ontem}`);
	console.log(`Consultando o offset ${offset}...`);
	let chamada;
	let lpcos;
	try {
		chamada = await get(tokens, `${urlConsulta}${offset}`);
		lpcos = chamada.body;

		console.log(`Salvando arquivo ${ontem}.json`);
		fs.writeFileSync(`./arquivos/${ontem}.json`, lpcos);
	} catch (e) {
		return console.log(`Erro ${e}, repita o offset ${offset}`);
	}

	const numeroTotalLpcos = chamada.headers['content-range'].split('/')[1];
	const paginasRestantes = Math.ceil((numeroTotalLpcos - (Number(chamada.headers['content-range'].split('-')[1].split('/')[0].trim()) - 1))/500);

	if (paginasRestantes > 0) {
		console.log(`${paginasRestantes} página(s) restante(s).`);
		for (let i = 1; i <= paginasRestantes; i++) {
			offset += 500;
			console.log(`Consultando o offset ${offset} página ${i} de ${paginasRestantes}...`);

			try {
				chamada = await get(tokens, `${urlConsulta}${offset}`);
				fs.writeFileSync(`./arquivos/${offset}.json`, lpcos);
			} catch (e) {
				console.log(`Erro ${e}, repita o offset ${offset}`)
			}
		}

	} else {
		return console.log('Extração completa');
	}
}

main();