exports.parametros = {
	"system-code": {
		// "hom": process.env.system_code_hom,
		"hom": "465041C41CDA180952CF5BB0030C3423",
		"prod": process.env.system_code_prod
	},
	"url-base": {
		"hom": 'https://hom-mdic.pucomex.serpro.gov.br',
		"prod": 'https://anuentes.portalunico.siscomex.gov.br'
	},
	"senha-certificado": {
		// "hom": process.env.senha_cert_hom,
		"hom": '123456',
		"prod": process.env.senha_cert_prod
	},
	"nome-certificado": {
		"hom": "ANUENTEGOVBR.pfx",
		"prod": "MDICGOVBR.p12"
	}
}