FROM node:12-alpine
WORKDIR /app
COPY . .
ENV ambiente hom
RUN npm install
EXPOSE 3000
CMD ["node", "helloPortalData.js"]